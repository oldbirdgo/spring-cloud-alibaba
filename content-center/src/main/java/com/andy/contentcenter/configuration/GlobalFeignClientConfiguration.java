package com.andy.contentcenter.configuration;

import feign.Logger;
import org.springframework.context.annotation.Bean;

/**
 * <B>系统名称：</B>学习<BR>
 * <B>模块名称：</B><BR>
 * <B>中文类名：</B>feign的配置类<BR>
 * <B>概要说明：</B>这个类别加@Configuration注解了，否则必须挪到@ComponentScan能扫描的包意外
 *                  <BR>
 *
 * @author wsr
 * @since Created in 2019/9/3 10:28
 */
public class GlobalFeignClientConfiguration {
    @Bean
    public Logger.Level level(){
        //让feign打印所有请求的细节
        return Logger.Level.FULL;
    }


}
