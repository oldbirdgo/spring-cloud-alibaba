package com.andy.contentcenter.mapper;

import com.andy.contentcenter.domain.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
