package com.andy.contentcenter.mapper;

import com.andy.contentcenter.domain.entity.MidUserShare;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户-分享中间表【描述用户购买的分享】 Mapper 接口
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
public interface MidUserShareMapper extends BaseMapper<MidUserShare> {

}
