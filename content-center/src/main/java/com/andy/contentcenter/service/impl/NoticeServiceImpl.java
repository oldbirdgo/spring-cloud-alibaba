package com.andy.contentcenter.service.impl;

import com.andy.contentcenter.domain.entity.Notice;
import com.andy.contentcenter.mapper.NoticeMapper;
import com.andy.contentcenter.service.NoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

}
