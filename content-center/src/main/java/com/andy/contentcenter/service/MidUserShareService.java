package com.andy.contentcenter.service;

import com.andy.contentcenter.domain.entity.MidUserShare;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户-分享中间表【描述用户购买的分享】 服务类
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
public interface MidUserShareService extends IService<MidUserShare> {

}
