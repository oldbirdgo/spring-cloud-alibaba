package com.andy.contentcenter.service;

import com.andy.contentcenter.domain.dto.ShareDTO;
import com.andy.contentcenter.domain.entity.Share;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 分享表 服务类
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
public interface ShareService extends IService<Share> {

     ShareDTO findById(Integer id);

}
