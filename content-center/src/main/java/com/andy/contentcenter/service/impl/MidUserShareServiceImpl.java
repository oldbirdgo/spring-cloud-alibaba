package com.andy.contentcenter.service.impl;

import com.andy.contentcenter.domain.entity.MidUserShare;
import com.andy.contentcenter.mapper.MidUserShareMapper;
import com.andy.contentcenter.service.MidUserShareService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户-分享中间表【描述用户购买的分享】 服务实现类
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
@Service
public class MidUserShareServiceImpl extends ServiceImpl<MidUserShareMapper, MidUserShare> implements MidUserShareService {

}
