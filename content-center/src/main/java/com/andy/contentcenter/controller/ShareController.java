package com.andy.contentcenter.controller;


import com.andy.contentcenter.domain.dto.ShareDTO;
import com.andy.contentcenter.domain.entity.Share;
import com.andy.contentcenter.mapper.ShareMapper;
import com.andy.contentcenter.service.ShareService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 分享表 前端控制器
 * </p>
 *
 * @author Andy
 * @since 2019-08-12
 */
@RestController
@RequestMapping("/share")
public class ShareController {

    @Autowired
    private ShareMapper shareMapper;
    @Autowired
    private ShareService shareService;
    @Autowired
    private DiscoveryClient discoveryClient;

    /**
     * 测试：服务发现，证明内容中心总能找到用户中心
     * @author wsr
     * @since 2019年8月28日12:04:23
     * @return 用户中心所有实例的地址信息
     */
    @GetMapping("/test2")
    public List<ServiceInstance> getInstances(){
        //查询指定服务的所有实例的信息
        //其余如consul/eureka/zookeeper..都可以用
       return this.discoveryClient.getInstances("user-center");
    }
    @GetMapping("/findByid")
    public ShareDTO findById(Integer id){
        return  this.shareService.findById(id);
    }

    @GetMapping("/test")
    public List<Share> testInsert(){
        Share share = new Share();
        share.setAuditStatus("xxxxx");
        share.setTitle("23132");
        share.setCreateTime(new Date());
        share.setUpdateTime(new Date());
        this.shareMapper.insert(share);
        QueryWrapper queryWrapper =new QueryWrapper();
        queryWrapper.eq("user_id",0);
        List<Share> list = this.shareMapper.selectList(queryWrapper);
        return list;
    }

}
