package com.alibaba.study.mysqlplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysqlPlusGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MysqlPlusGeneratorApplication.class, args);
    }

}
