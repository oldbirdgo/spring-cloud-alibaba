package com.andy.usercenter.mapper;

import com.andy.usercenter.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分享 Mapper 接口
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
public interface UserMapper extends BaseMapper<User> {

}
