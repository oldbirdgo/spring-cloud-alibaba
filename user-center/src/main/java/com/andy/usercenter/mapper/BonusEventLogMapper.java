package com.andy.usercenter.mapper;

import com.andy.usercenter.entity.BonusEventLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分变更记录表 Mapper 接口
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
public interface BonusEventLogMapper extends BaseMapper<BonusEventLog> {

}
