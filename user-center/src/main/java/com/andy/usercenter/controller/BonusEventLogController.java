package com.andy.usercenter.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 积分变更记录表 前端控制器
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
@RestController
@RequestMapping("/bonus-event-log")
public class BonusEventLogController {

}
