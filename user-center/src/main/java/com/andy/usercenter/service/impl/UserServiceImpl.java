package com.andy.usercenter.service.impl;

import com.andy.usercenter.entity.User;
import com.andy.usercenter.mapper.UserMapper;
import com.andy.usercenter.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分享 服务实现类
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
