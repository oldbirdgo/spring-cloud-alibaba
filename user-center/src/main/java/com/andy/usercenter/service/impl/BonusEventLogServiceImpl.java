package com.andy.usercenter.service.impl;

import com.andy.usercenter.entity.BonusEventLog;
import com.andy.usercenter.mapper.BonusEventLogMapper;
import com.andy.usercenter.service.BonusEventLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分变更记录表 服务实现类
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
@Service
public class BonusEventLogServiceImpl extends ServiceImpl<BonusEventLogMapper, BonusEventLog> implements BonusEventLogService {

}
