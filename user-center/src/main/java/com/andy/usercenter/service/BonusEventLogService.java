package com.andy.usercenter.service;

import com.andy.usercenter.entity.BonusEventLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 积分变更记录表 服务类
 * </p>
 *
 * @author Andy
 * @since 2019-08-11
 */
public interface BonusEventLogService extends IService<BonusEventLog> {

}
